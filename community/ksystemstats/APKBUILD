# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=ksystemstats
pkgver=5.27.7
pkgrel=1
pkgdesc="A plugin based system monitoring daemon"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LicenseRef-KDE-Accepted-GPL AND LicenseRef-KDE-Accepted-LGPL AND CC0-1.0"
makedepends="
	eudev-dev
	extra-cmake-modules
	kcoreaddons-dev
	kdbusaddons-dev
	kio-dev
	libksysguard-dev
	libnl3-dev
	lm-sensors-dev
	networkmanager-qt-dev
	qt5-qtbase-dev
	samurai
	solid-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/ksystemstats.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/ksystemstats-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Broken, requires specific sensor setup

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/ksystemstats.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
58dc0e61294ef23d1e4feb430872f47bb90e544cc12163dba917e8bcce5b9aa1248347203488c530eb56a56d41a3631ba3c971edda5b17ed3e74cddd81083625  ksystemstats-5.27.7.tar.xz
"
