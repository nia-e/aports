# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kiconthemes
pkgver=5.108.0
pkgrel=2
pkgdesc="Support for icon themes"
# armhf blocked by extra-cmake-module
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends_dev="
	karchive-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kitemviews-dev
	kwidgetsaddons-dev
	qt5-qtsvg-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kiconthemes.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kiconthemes-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kiconthemes.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# kiconloader_unittest is broken
	xvfb-run ctest --test-dir build --output-on-failure -E "(kiconloader_unittest|kiconloader_resourcethemetest)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5840044402d7ccfc903cc8ace679e4169fe494cb3248e603a827ae228bc80e40b4c530b7bdf5b45edb18cd98edab8112c74b0c96e6f9e15947f0561fa5980be6  kiconthemes-5.108.0.tar.xz
"
