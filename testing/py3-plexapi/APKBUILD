# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=py3-plexapi
_pkgname=python-plexapi
pkgver=4.15.0
pkgrel=0
pkgdesc="Python bindings for the Plex API"
url="https://github.com/pkkid/python-plexapi"
arch="noarch"
license="BSD-3-Clause"
# tests requires an instance of plex running
# net for sphinx
options="net !check"
depends="
	python3
	py3-requests
	"
makedepends="
	py3-gpep517
	py3-recommonmark
	py3-setuptools
	py3-sphinx_rtd_theme
	py3-wheel
	"
subpackages="$pkgname-doc $pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/pkkid/python-plexapi/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
	sphinx-build -W -b man docs man
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
	install -Dm644 man/pythonplexapi.1 -t "$pkgdir"/usr/share/man/man1
}

sha512sums="
ca66e4e6693571c3495de13ff9180c23db5675d9ab62753a6a09d3a67cf162fd75d8b0f77c36834089aaa2307051822584723b4b0ba69ccc4c4eb76949e7900e  py3-plexapi-4.15.0.tar.gz
"
