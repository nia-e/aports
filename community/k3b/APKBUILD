# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=k3b
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/multimedia/org.kde.k3b"
pkgdesc="A full-featured CD/DVD/Blu-ray burning and ripping application"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="
	cdrdao
	dvd+rw-tools
	libburn
	"
makedepends="
	extra-cmake-modules
	flac-dev
	karchive-dev
	kcmutils-dev
	kconfig-dev
	kcoreaddons-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kservice-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	lame-dev
	libdvdread-dev
	libkcddb-dev
	libmad-dev
	libsamplerate-dev
	libvorbis-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	solid-dev
	taglib-dev
	"
_repo_url="https://invent.kde.org/multimedia/k3b.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/k3b-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DK3B_BUILD_MUSE_DECODER_PLUGIN=OFF \
		-DK3B_BUILD_SNDFILE_DECODER_PLUGIN=OFF \
		-DK3B_ENABLE_MUSICBRAINZ=OFF
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
de2d1eb7176226751658af688427cd2d2daf038d0dd5941769639a08ddf22064b062b3709f07e72d788c1f19cb15250d8d240ed0edb148e21dcdf4454fcbbcf6  k3b-23.04.3.tar.xz
"
