# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=layer-shell-qt
pkgver=5.27.7
pkgrel=1
pkgdesc="Qt component to allow applications to make use of the Wayland wl-layer-shell protocol"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="
	qt5-qtdeclarative-dev
	qt5-qtwayland-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/layer-shell-qt.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/layer-shell-qt-$pkgver.tar.xz"
subpackages="$pkgname-dev"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/layer-shell-qt.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DENABLE_TESTING=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3f03f9850a95b05c6f6c194552127feb0af7830e705eb5d8497b0793f6d1165d2e73ba99657aea3f312d98dca57f98b7d3a359c09d8afda68fa5fe02619287aa  layer-shell-qt-5.27.7.tar.xz
"
