# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker-cli-compose
pkgver=2.20.3
pkgrel=0
pkgdesc="Docker CLI plugin for extended build capabilities"
url="https://docs.docker.com/compose/cli-command"
arch="all"
license="Apache-2.0"
depends="docker-cli"
makedepends="go"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/docker/compose/archive/v$pkgver.tar.gz"

provides="docker-compose=$pkgver-r$pkgrel"

# secfixes:
#   2.15.1-r0:
#     - CVE-2022-27664
#     - CVE-2022-32149
#   2.12.1-r0:
#     - CVE-2022-39253

_plugin_installdir="/usr/libexec/docker/cli-plugins"

builddir="$srcdir"/compose-"$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	PKG=github.com/docker/compose/v2
	local ldflags="-X '$PKG/internal.Version=v$pkgver'"
	go build -ldflags="$ldflags" -o docker-compose ./cmd
}

check() {
	# e2e tests are excluded because they depend on live dockerd/kubernetes/ecs
	local pkgs="$(go list ./... | grep -Ev '/(watch|e2e)(/|$)')"
	go test -short $pkgs
	./docker-compose version
}

package() {
	install -Dm755 docker-compose -t "$pkgdir$_plugin_installdir"/

	mkdir -p "$pkgdir"/usr/bin
	ln -sfv ../libexec/docker/cli-plugins/docker-compose "$pkgdir"/usr/bin/docker-compose
}

sha512sums="
d533318bbe550568a2c5bd43c6fa6d1d9b3887f09b6d75f278687dc28d23c4863c618599a816b7789a19182acaf869b6785c4bfc3a9b26d6329f1ac97b8059cf  docker-cli-compose-2.20.3.tar.gz
"
