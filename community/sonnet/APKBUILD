# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=sonnet
pkgver=5.108.0
pkgrel=2
pkgdesc="Spelling framework for Qt5"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends="hunspell"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	hunspell-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/sonnet.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/sonnet-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/sonnet.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# sonnet-test_autodetect fails to detect a speller backend
	xvfb-run ctest --test-dir build --output-on-failure -E "sonnet-test_autodetect"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5f8d8d95bdc180ff5b02b0a39094c37fd1dc580b6458f9c1adacaf9880d9058822c33f3ef95926801fe2719b80a4f9a0bd25e00ad24303ea09d123e889219784  sonnet-5.108.0.tar.xz
"
