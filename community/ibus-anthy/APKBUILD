# Maintainer:
pkgname=ibus-anthy
pkgver=1.5.14
pkgrel=2
pkgdesc="Japanese input method Anthy IMEngine for IBus Framework"
url="https://github.com/ibus/ibus-anthy"
arch="all !s390x" # ibus needs librsvg which isn't available on s390x
license="GPL-2.0-only"
depends="
	ibus
	py3-gobject3
	"
makedepends="
	anthy-dev
	autoconf
	automake
	gobject-introspection-dev
	ibus-dev
	intltool
	m4
	"
checkdepends="
	bash
	libfaketime
	sed
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/ibus/ibus-anthy/archive/$pkgver.tar.gz"
subpackages="$pkgname-lang"

prepare() {
	default_prepare

	NOCONFIGURE=1 ./autogen.sh
}

build() {
	./configure \
		--prefix=/usr \
		--host=$CHOST \
		--build=$CBUILD \
		--libexec=/usr/lib/ibus
	make
}

check() {
	# tests use current date output and need yearly updates
	# they also run rm -r HOME/.config/anthy ...
	HOME="$builddir" \
	faketime '2021-01-01' \
		make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
4d06d4e177df62d22c61bf6ebfda04ddbf545ea1947d0c315e3dfe5e48d9ddc57e5fcc89919ae3b9f7430e6124a5dfb1295f4ca7362fbc35714d627f80fae6d8  ibus-anthy-1.5.14.tar.gz
"
