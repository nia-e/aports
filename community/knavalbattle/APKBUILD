# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=knavalbattle
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/knavalbattle/"
pkgdesc="A ship sinking game"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcrash-dev
	kdbusaddons-dev
	kdnssd-dev
	kdoctools-dev
	ki18n-dev
	ktextwidgets-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/knavalbattle.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/knavalbattle-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7bd5805421f3745697a2f6f76329c08715a4899754d8a966a01c00bec2b9a01809d91172cbc3cb6b4449a4f6c5ed0cde59ac6159ebd69950e61377edc4444cb7  knavalbattle-23.04.3.tar.xz
"
