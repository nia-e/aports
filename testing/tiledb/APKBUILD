# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: Holger Jaekel <holger.jaekel@gmx.de>
pkgname=tiledb
pkgver=2.16.2
pkgrel=1
pkgdesc="Engine for storing and accessing dense and sparse multi-dimensional arrays"
url="https://tiledb.com/"
# Tests fail on s390x
# doesn't build on 32-bit
arch="all !armhf !armv7 !x86 !s390x"
license="MIT"
makedepends="
	abseil-cpp-dev
	blosc-dev
	bzip2-dev
	capnproto-dev
	catch2-3
	clang
	cmake
	crc32c-dev
	curl-dev
	doxygen
	file-dev
	google-cloud-cpp-dev
	libpng-dev
	lz4-dev
	nlohmann-json
	openssl-dev
	samurai
	spdlog-dev
	zlib-dev
	zstd-dev
	"
subpackages="
	$pkgname-dev
	"
source="tiledb-$pkgver.tar.gz::https://github.com/TileDB-Inc/TileDB/archive/refs/tags/$pkgver.tar.gz
	https://raw.githubusercontent.com/muellan/clipp/v1.2.3/include/clipp.h
	30-versions.patch
	40-catch.patch
	50-magic.patch
	60-clipp.patch
	70-crc32c.patch
	"
builddir="$srcdir/TileDB-$pkgver"
options="!check" # fail to build on 2.15

# Optional dependencies aws-* are not available on s390x and ppc64le
_with_s3="OFF"
case "$CARCH" in
arm*|s390x|ppc64le) ;;
*)
	makedepends="$makedepends aws-crt-cpp-dev aws-sdk-cpp-dev"
	_with_s3="ON"
	;;
esac

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CC=clang \
	CXX=clang++ \
	CXXFLAGS="$CXXFLAGS -Wno-deprecated-declarations" \
	LDFLAGS="$LDFLAGS -Wl,--copy-dt-needed-entries" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_SKIP_RPATH=ON \
		-DTILEDB_SUPERBUILD=OFF \
		-DTILEDB_VERBOSE=OFF \
		-DTILEDB_HDFS=OFF \
		-DTILEDB_S3=$_with_s3 \
		-DTILEDB_AZURE=OFF \
		-DTILEDB_GCS=ON \
		-DTILEDB_SERIALIZATION=ON \
		-DTILEDB_TOOLS=OFF \
		-DTILEDB_WERROR=OFF \
		-DTILEDB_CPP_API=ON \
		-DTILEDB_STATS=ON \
		-DTILEDB_STATIC=OFF \
		-DTILEDB_TESTS="$(want_check && echo ON || echo OFF)" \
		-DTILEDB_CCACHE=OFF \
		-DTILEDB_ARROW_TESTS=OFF \
		-DTILEDB_CRC32=ON \
		-DTILEDB_WEBP=OFF \
		-DTILEDB_FORCE_ALL_DEPS=OFF \
		$CMAKE_CROSSOPTS

	# compile Cap’n Proto schema with the current version
	cd "$builddir/tiledb/sm/serialization"
	capnp compile -oc++:posix tiledb-rest.capnp
	cd "$builddir"

	cmake --build build
	if want_check; then
		cmake --build build --target tests
	fi
}

check() {
	ctest --output-on-failure --test-dir build \
		-R '^unit_|test_assert' -E 'unit_compressors|unit_delete_update_condition'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7b8ac64f8e82185543253fc05aa2b15fa754652122fd0cadbf8a203050b5dcd81d29e43a421d535d28bee33155e2837f48a5094e3797c1c73400c29603b1fce1  tiledb-2.16.2.tar.gz
0a801eff46581a96e571b41ce734d5a8e7f30333d46e3fb81c4c40fc44c18035d4b4597732221e37945ad23b94e53e496562ae4b24d358761dbf5ed803f66ff4  clipp.h
ef83c2568a3ec826d4cf03fd2b40d7420568457bca5dc2a78651ecc25c76c84d6fa9ed2e54bd91764d6fa490e89fb5972772a01f210fdac212af2f6eae22d472  30-versions.patch
999351a80fae0163c51ff57ac70d03e02318e8e5514179602afd7a8bee8e04eefd18fbd9b58d9f7c9d1b2754a2c75f5364d1f5f1e50696c8f2e3476f7d204bea  40-catch.patch
0447a8ba37c3befd5f7e003165155b2720926b8a73711afc68828692ea31e4dd59c5e729934dfcd621874602fa31d4c9e78f8cedd8bfc50f5be547acd51907fb  50-magic.patch
55620d05d18d213072f58a582677390e814a4ffd70e3e0328da4488a9c67fd06edd938ca0102d39dc763e9e665fc8a057b63f18d7c3816361bb1e186d413fd82  60-clipp.patch
fd139d2ce53a0c7a1a06dfaf6d79967778f611653c4ece9873a50ed0b0003276fc07496348818a18385da53e89a08e90a47e02fd1630088315df5c69e52bd73f  70-crc32c.patch
"
