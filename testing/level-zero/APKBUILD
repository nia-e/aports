# Contributor: Leon Marz <main@lmarz.org>
# Maintainer: Leon Marz <main@lmarz.org>
pkgname=level-zero
pkgver=1.13.5
pkgrel=0
pkgdesc="oneAPI Level Zero Loader"
url="https://spec.oneapi.com/versions/latest/elements/l0/source/index.html"
arch="all"
license="MIT"
makedepends="cmake samurai"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/oneapi-src/level-zero/archive/v$pkgver.tar.gz"
options="!check" # no testsuite

build() {
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=ON \
		-DCMAKE_BUILD_TYPE=None

	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev
	amove usr/lib/libze_tracing_layer.so*
	amove usr/lib/libze_validation_layer.so*
}

sha512sums="
1b1cfec12f06e1095f17c21d3deffec2645b8bdca24d62594602c896a355627c24185415e77e2a4c540316be5b851727ea4619282333ff0f0ad0e8724f726ee6  level-zero-1.13.5.tar.gz
"
