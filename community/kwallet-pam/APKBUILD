# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kwallet-pam
pkgver=5.27.7
pkgrel=1
pkgdesc="KWallet PAM integration"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later"
depends="socat"
makedepends="
	extra-cmake-modules
	kwallet-dev
	libgcrypt-dev
	linux-pam-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kwallet-pam.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kwallet-pam-$pkgver.tar.xz"
options="!check" # No tests available

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/kwallet-pam.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/ \
		-DCMAKE_INSTALL_LIBEXECDIR=usr/libexec \
		-DCMAKE_INSTALL_LIBDIR=lib # for the pam module to be in /lib/security
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/lib/systemd
}

sha512sums="
1af8393e4b2e7d8443fb8ca203a24145ca61f7c8eb850500a0106b0cc7a881b67eca0cb7e0acbe41d451b7c588c8213d36feb3402c846373c609f6d240dc08bc  kwallet-pam-5.27.7.tar.xz
"
