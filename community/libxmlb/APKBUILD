# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Pablo Correa Gomez <ablocorrea@hotmail.com>
pkgname=libxmlb
pkgver=0.3.12
pkgrel=0
pkgdesc="Library to help create and query binary XML blobs"
url="https://github.com/hughsie/libxmlb"
arch="all"
license="LGPL-2.1-or-later"
makedepends="meson glib-dev gobject-introspection-dev xz-dev zstd-dev"
checkdepends="shared-mime-info"
subpackages="$pkgname-dev:_dev $pkgname-dbg $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/hughsie/libxmlb/archive/$pkgver.tar.gz"

build() {
	abuild-meson \
		-Dgtkdoc=false \
		-Dtests=true \
		-Dstemmer=false \
		-Dintrospection=true \
		output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
	rm -rf "$pkgdir"/usr/share/installed-tests
	rm -rf "$pkgdir"/usr/libexec/installed-tests
}

_dev() {
	default_dev

	amove usr/bin/xb-tool
}

sha512sums="
0ad5fa43b3ae75bfcd52dd463df1e98723e2ef5e721dc6860d842e9155b213ccdd8c2c9eb211db512880e183a8d96f8256a2a289a6593634bd63040aa2ead1b9  libxmlb-0.3.12.tar.gz
"
