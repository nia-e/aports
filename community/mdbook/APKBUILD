# Maintainer: crapStone <crapstone01@gmail.com>
pkgname=mdbook
pkgver=0.4.34
pkgrel=0
pkgdesc="mdBook is a utility to create modern online books from Markdown files"
url="https://rust-lang.github.io/mdBook/"
arch="all"
license="MPL-2.0"
makedepends="rust cargo cargo-auditable"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/rust-lang/mdBook/archive/v$pkgver.tar.gz"
builddir="$srcdir/mdBook-$pkgver"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release

	./target/release/mdbook completions bash > $pkgname.bash
	./target/release/mdbook completions fish > $pkgname.fish
	./target/release/mdbook completions zsh > $pkgname.zsh
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/mdbook "$pkgdir"/usr/bin/mdbook

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
f3adbd165fcb9dad9175cc5f4287a6e4a2ecc58dcfca1f532f20ad68332ab85eff469834718971bb68103ad4b524a4cd3aabf3c24df9a24c2cdbdcfb9c56cd73  mdbook-0.4.34.tar.gz
"
