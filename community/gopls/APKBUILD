# Contributor: David Florness <david@florness.com>
# Maintainer: David Florness <david@florness.com>
pkgname=gopls
pkgver=0.13.1
pkgrel=2
pkgdesc="Language server for Go programming language"
url="https://github.com/golang/tools/blob/master/gopls"
license="BSD-3-Clause"
arch="all"
options="chmod-clean"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/golang/tools/archive/gopls/v$pkgver.tar.gz"
builddir="$srcdir/tools-$pkgname-v$pkgver/$pkgname"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -trimpath -o dist/gopls
}

check() {
	cd test
	go test
}

package() {
	install -Dm755 dist/gopls "$pkgdir"/usr/bin/gopls
}

sha512sums="
c9f184e99f6599956d98c871daf0fbbab2fcc3bdf1616edce1ab361a0052b8653d3bad04306f6c08bfcccda9e46f42ca110b9284b3c31d518b5313d291ea2852  gopls-0.13.1.tar.gz
"
