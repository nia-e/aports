# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=lego
pkgver=4.13.3
pkgrel=1
pkgdesc="Let's Encrypt client and ACME library written in Go"
url="https://github.com/go-acme/lego"
license="MIT"
arch="all !s390x" # tests fail due to network timeouts
options="net setcap chmod-clean" # tests need network access: https://github.com/go-acme/lego/issues/560
depends="ca-certificates"
makedepends="go libcap-utils"
checkdepends="tzdata"
source="https://github.com/go-acme/lego/archive/v$pkgver/lego-$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

case "$CARCH" in
	aarch64)
		# TestChallengeWithProxy/matching_X-Forwarded-Host_(multiple_fields)
		options="$options !check"
		;;
	ppc64le)
		# timeout on doing stuff with a server halfway across the world
		options="$options !check"
		;;
esac

build() {
	go build -v -ldflags "-X main.version=$pkgver" -o ./bin/lego ./cmd/lego
}

check() {
	go test ./...
}

package() {
	install -Dm755 ./bin/lego "$pkgdir"/usr/bin/lego
	setcap cap_net_bind_service=+ep "$pkgdir"/usr/bin/lego
}

sha512sums="
e7cd6742a165069d7e90fc18aa6e487653cde33dc709b6f6b22ff9f8cdf3d0b5167a170ab49b1576d08114504d72e5cc49a0eaad23033b972080e9f7f1f364cd  lego-4.13.3.tar.gz
"
